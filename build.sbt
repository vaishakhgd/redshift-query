name := "RedShiftQuery"

version := "0.1"

scalaVersion := "2.12.8"

val AkkaVersion = "2.6.8"

libraryDependencies +=
  "com.typesafe.akka" %% "akka-actor" % AkkaVersion

libraryDependencies +=
  "com.typesafe.akka" %% "akka-remote" % AkkaVersion

libraryDependencies += "com.typesafe.akka" %% "akka-cluster-tools" % AkkaVersion

libraryDependencies += "com.typesafe.akka" %% "akka-cluster" % AkkaVersion

libraryDependencies += "com.typesafe.akka" %% "akka-cluster-typed" % AkkaVersion

libraryDependencies += "ch.qos.logback" % "logback-classic" % "1.1.3" % Runtime

libraryDependencies += "com.typesafe.akka" %% "akka-http"   % "10.1.12"


val sparkVersion = "2.2.0"

lazy val sparkCore = "org.apache.spark" %% "spark-core" % "3.0.0"

lazy val sparkSql = "org.apache.spark" %% "spark-sql" % "3.0.0"
// https://mvnrepository.com/artifact/org.apache.spark/spark-mllib
lazy val sparkMLlib = "org.apache.spark" %% "spark-mllib" %  "3.0.0"

resolvers += "jitpack" at "https://jitpack.io"
resolvers += "redshift" at "http://redshift-maven-repository.s3-website-us-east-1.amazonaws.com/release"



libraryDependencies += sparkCore
libraryDependencies += sparkMLlib
libraryDependencies += sparkSql

// https://mvnrepository.com/artifact/com.amazonaws/aws-java-sdk-s3
libraryDependencies += "com.amazonaws" % "aws-java-sdk-s3" % "1.10.6"

// https://mvnrepository.com/artifact/com.amazonaws/aws-java-sdk-redshift
libraryDependencies += "com.amazonaws" % "aws-java-sdk-redshift" % "1.11.841"

libraryDependencies += "com.amazonaws" % "aws-java-sdk-core" % "1.11.841"

 libraryDependencies += "com.amazonaws" % "aws-java-sdk-sts" % "1.11.841"




libraryDependencies += "org.apache.hadoop" % "hadoop-aws" % "3.0.0"

libraryDependencies += "org.apache.hadoop" % "hadoop-common" % "3.0.0"

libraryDependencies += "org.apache.hadoop" % "hadoop-client" % "3.0.0"

libraryDependencies += "com.typesafe.akka" %% "akka-http-spray-json" % "10.1.12"


libraryDependencies += "org.apache.httpcomponents" % "httpclient" % "4.5.12"



libraryDependencies += "org.scalikejdbc" %% "scalikejdbc"                  % "3.2.0"
libraryDependencies += "org.scalikejdbc" %% "scalikejdbc-config"           % "3.2.0"
libraryDependencies += "org.scalikejdbc" %% "scalikejdbc-play-initializer" % "2.6.0-scalikejdbc-3.2"

//libraryDependencies += "com.amazon.redshift" % "redshift-jdbc4" % "1.2.10.1009"

// https://mvnrepository.com/artifact/com.amazon.redshift/redshift-jdbc42
libraryDependencies += "com.amazon.redshift" % "redshift-jdbc42" % "1.2.1.1001"

// https://mvnrepository.com/artifact/com.databricks/spark-redshift
// https://mvnrepository.com/artifact/com.databricks/spark-redshift

val AkkaHttpVersion = "10.2.0"
libraryDependencies += "com.typesafe.akka" %% "akka-http-spray-json" % AkkaHttpVersion

libraryDependencies ++= Seq(
 "com.typesafe.slick" %% "slick"           % "3.3.2",
 "com.h2database"      % "h2"              % "1.4.200",
 "ch.qos.logback"      % "logback-classic" % "1.2.3"
)



// https://mvnrepository.com/artifact/com.databricks/spark-redshift





