package controller

import akka.actor.typed.ActorSystem
import akka.actor.typed.scaladsl.Behaviors
import akka.http.scaladsl.Http
import akka.Done
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.model.StatusCodes
import akka.stream.ActorMaterializer
import database.{CreateTable, RedShiftPOC}
import database.orm.CreateTableDetails
import spray.json.{DefaultJsonProtocol, RootJsonFormat}
// for JSON serialization/deserialization following dependency is required:
// "com.typesafe.akka" %% "akka-http-spray-json" % "10.1.7"
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import spray.json.DefaultJsonProtocol._

import scala.io.StdIn

import scala.concurrent.Future

import spray.json.RootJsonFormat
import spray.json.DefaultJsonProtocol._
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._

import akka.http.scaladsl.server.Directives._
import akka.actor.typed.scaladsl.adapter._
import akka.http.scaladsl.Http.ServerBinding
import akka.http.scaladsl.model._
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Route

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import spray.json.RootJsonFormat
import spray.json.DefaultJsonProtocol._


object User1{

}
import akka.http.scaladsl.server.{ Directives, Route }
object SprayJsonExample2  {



  var orders: List[Item] = Nil


  final case class Item(name: String, id: Long)
  final case class Order(items: List[Item])


  import DefaultJsonProtocol._

  implicit val orderFormat = jsonFormat9(CreateTableDetails)

  implicit val system = ActorSystem(Behaviors.empty, "SprayExample")
  implicit val executionContext = system.executionContext

  def fetchItem(itemId: Long): Future[Option[Item]] = Future {
    orders.find(o => o.id == itemId)
  }
  def saveOrder(order: Order): Future[Done] = {
    orders = order match {
      case Order(items) => items ::: orders
      case _            => orders
    }
    Future { Done }
  }

  case class DetailsJsonEntity(DetailsJson : String)
  implicit val detailsJsonEntityFormat = jsonFormat1(DetailsJsonEntity)

  def main(args: Array[String]): Unit = {
    val route: Route =
      concat(
        get{
          path("creategroup2"){

            entity(as[CreateTableDetails]){ tableDetails : CreateTableDetails =>
              val latLngList = RedShiftPOC.latLngCoordinatesFour(latitude = tableDetails.latitude,longitude = tableDetails.longitude )
              val latlng = RedShiftPOC.getNearestLatLng(latitude = tableDetails.latitude,longitude = tableDetails.longitude,latLngList)

              val saved  = CreateTable.createTable(latlng.latitude,latlng.longitude,tableDetails)
              // we are not interested in the result value `Done` but only in the fact that it was successful
              complete("order created")


            }
          }
        },
        post {
          path("YOPO2"){
        parameters("DetailsJson".as[String],
          "latitude".as[Double],
          "longitude".as[Double],
          "countrycode".as[String],
          "phonenumber".as[String]
          ,"groupname".as[String],
          "deviceToken".as[String],
          "groupid".as[String],
          "isVisible".as[String]
        ){
          (DetailsJson,latitude,longitude,countrycode,phonenumber,groupname,deviceToken,isVisible,groupid) => {

            val createTableDetails = CreateTableDetails(phoneno = phonenumber,countrycode = countrycode,latitude = latitude,
              longitude = longitude,groupname= groupname,detailsjson = DetailsJson,groupid = groupid,devicetoken = deviceToken,isvalid = isVisible)


            val latLngList = RedShiftPOC.latLngCoordinatesFour(latitude = createTableDetails.latitude,longitude = createTableDetails.longitude )
            println("MEGHGGHGHHH")
            latLngList.foreach(println)
            println("MEGHGGHGHHH")
            val latlng = RedShiftPOC.getNearestLatLng(latitude = createTableDetails.latitude,longitude = createTableDetails.longitude,latLngList)

            val saved  = CreateTable.createTable(latlng.latitude,latlng.longitude,createTableDetails)
            // we are not interested in the result value `Done` but only in the fact that it was successful

            complete("Received")

          }

        }

        // complete("QUOUMBO")
      }
    },
        post{
          path("Yopo3"){
            entity(as[CreateTableDetails]){
              tableDetails =>
                val latLngList = RedShiftPOC.latLngCoordinatesFour(latitude = tableDetails.latitude,longitude = tableDetails.longitude )
                val latlng = RedShiftPOC.getNearestLatLng(latitude = tableDetails.latitude,longitude = tableDetails.longitude,latLngList)

                val saved  = CreateTable.createTable(latlng.latitude,latlng.longitude,tableDetails)
                complete(tableDetails.toString)
            }
          }
        }
      )

   // val bindingFuture = Http().newServerAt("0.0.0.0", 8080).bind(route)

    Http().newServerAt("0.0.0.0",8080).bind(route)
  //  Http().bindAndHandle(MainRouter.routes, "0.0.0.0",  8080)
    println(s"Server online at http://localhost:8080/\nPress RETURN to stop...")

  }
}
