package database

import scala.collection.mutable.ListBuffer

case class LatLng(longitude : Int,latitude : Int)

object RedShiftPOC {


  def getNearestLatLng(latitude : Double,longitude :Double, listLatLng : List[LatLng]) : LatLng ={

    var distance = Long.MaxValue

    var nearestLatLng =  LatLng(0,0)

    try {

      nearestLatLng = listLatLng.apply(0)

      for( latlng  <- listLatLng){

        var d = RadiusCalculatePOC.getRadius(latitude,longitude,latlng.latitude.toDouble,latlng.longitude.toDouble)

        println("Yoo  " + latlng.latitude + "," + latlng.longitude)

        println("D is "+d)

        if(d < distance){
          distance = d
          nearestLatLng= latlng
        }

      }
    }
    catch {
      case e : Exception => e.printStackTrace()
    }


    nearestLatLng

  }

  def latLngCoordinatesFour(latitude : Double, longitude : Double) : List[LatLng] ={
    print("Yo")
    var latList = new ListBuffer[Int]()
    var longList = new ListBuffer[Int]()
    val latitudeIntFloor  = Math.floor(latitude).toInt
    val latitudeIntCeil  = Math.ceil(latitude).toInt

    println("latitudeIntFloor "+ latitudeIntFloor)
    println("latitudeIntCeil "+ latitudeIntCeil)


    if(latitudeIntFloor <=90 && latitudeIntFloor>= -90){
      latList+=latitudeIntFloor
    }
    if(latitudeIntCeil <=90 && latitudeIntCeil>= -90){
      latList+=latitudeIntCeil
    }

    val longitudeIntCeil = Math.ceil(longitude).toInt
    val longitudeIntFloor = Math.floor(longitude).toInt

    println("longitudeIntCeil "+ longitudeIntCeil)
    println("longitudeIntFloor "+ longitudeIntFloor)

    if(longitudeIntCeil > 180){
      longList += -180
    }
    if(longitudeIntFloor > 180){
      longList += -180
    }
    if(longitudeIntCeil < -180){
      longList+=180
    }
    if(longitudeIntFloor < -180){
      longList+=180
    }
    if(longitudeIntFloor<=180 && longitudeIntFloor>= -180){
      longList += longitudeIntFloor
    }
    if(longitudeIntCeil<=180 && longitudeIntCeil>= -180){
      longList += longitudeIntCeil
    }

   var latListNew  = latList.toList
    var longListNew = longList.toList

    latListNew.foreach(println)

    println("----------------------------")
    longListNew.foreach(println)


    var latlngList = new ListBuffer[LatLng]

    for( lat  <-  0 to (latListNew.size -1) ) {
      for(long  <- 0 to (longListNew.size -1 )){

        println(latListNew.apply(lat) + ",," + longListNew.apply(long))
        var latLng = LatLng(latitude = latListNew.apply(lat),longitude = longListNew.apply(long))
        //println(latLng.latitude + "," + latLng.longitude)
        latlngList+=latLng
      }
    }

    latlngList.toList
  }
}
