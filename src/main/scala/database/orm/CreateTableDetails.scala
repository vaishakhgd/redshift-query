package database.orm

final case class CreateTableDetails(phoneno : String,countrycode : String,
                              latitude : Double,longitude : Double,
                              groupname : String,detailsjson : String,
                              groupid : String,devicetoken : String,isvalid : String)


