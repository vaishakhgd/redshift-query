package redshift

import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.services.redshift.AmazonRedshiftClient

object RedShift {

  def createRedShift() ={
    val credentials = new BasicAWSCredentials(AWSCredentials.getAccessKey(),AWSCredentials.getSecretKey())
   val client = new AmazonRedshiftClient(credentials);
    client.setEndpoint("https://redshift.us-east-1.amazonaws.com/");
  }

}
